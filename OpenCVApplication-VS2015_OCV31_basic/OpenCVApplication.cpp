// OpenCVApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <queue> 
#include "common.h"
#include "fstream"
using namespace std;
struct Neighbor {
	int label;
	double distance;
	Neighbor(int label, double distance)
		:label(label), distance(distance)
	{
	}
	bool operator <(const Neighbor& b) const
	{
		return distance < b.distance;
	}
	
};
struct MyP {
	int x, y, z;
	MyP(int x, int y, int z)
		:x(x), y(y), z(z)
	{

	}
	bool operator==(const MyP& rhs)
	{
		return x == rhs.x && y == rhs.y && z == rhs.z;
	}
	MyP operator-(const MyP& b)
	{
		return MyP(x - b.x, y - b.y, z - b.z);
	}
};
struct comparer {
	bool operator()(Neighbor const& p1, Neighbor const& p2)
	{
		// return "true" if "p1" is ordered  
		// before "p2", for example: 
		return p1.distance < p2.distance;
	}
};
struct peak{
	int theta, ro, hval;
	bool operator < (const peak& o) const {
		return hval > o.hval;
	}
};
bool compareByLength(const peak &a, const peak &b)
{
	return a.hval > b.hval;
}
void testOpenImage()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src;
		src = imread(fname);
		imshow("image", src);
		waitKey();
	}
}

void testOpenImagesFld()
{
	char folderName[MAX_PATH];
	if (openFolderDlg(folderName) == 0)
		return;
	char fname[MAX_PATH];
	FileGetter fg(folderName, "bmp");
	while (fg.getNextAbsFile(fname))
	{
		Mat src;
		src = imread(fname);
		imshow(fg.getFoundFileName(), src);
		if (waitKey() == 27) //ESC pressed
			break;
	}
}

void testImageOpenAndSave()
{
	Mat src, dst;

	src = imread("Images/Lena_24bits.bmp", CV_LOAD_IMAGE_COLOR);	// Read the image

	if (!src.data)	// Check for invalid input
	{
		printf("Could not open or find the image\n");
		return;
	}

	// Get the image resolution
	Size src_size = Size(src.cols, src.rows);

	// Display window
	const char* WIN_SRC = "Src"; //window for the source image
	namedWindow(WIN_SRC, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_SRC, 0, 0);

	const char* WIN_DST = "Dst"; //window for the destination (processed) image
	namedWindow(WIN_DST, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_DST, src_size.width + 10, 0);

	cvtColor(src, dst, CV_BGR2GRAY); //converts the source image to a grayscale one

	imwrite("Images/Lena_24bits_gray.bmp", dst); //writes the destination to file

	imshow(WIN_SRC, src);
	imshow(WIN_DST, dst);

	printf("Press any key to continue ...\n");
	waitKey(0);
}

void testNegativeImage()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		double t = (double)getTickCount(); // Get the current time [s]

		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat dst = Mat(height, width, CV_8UC1);
		// Asa se acceseaaza pixelii individuali pt. o imagine cu 8 biti/pixel
		// Varianta ineficienta (lenta)
		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				uchar val = src.at<uchar>(i, j);
				uchar neg = 255 - val;
				dst.at<uchar>(i, j) = neg;
			}
		}

		// Get the current time again and compute the time difference [s]
		t = ((double)getTickCount() - t) / getTickFrequency();
		// Print (in the console window) the processing time in [ms] 
		printf("Time = %.3f [ms]\n", t * 1000);

		imshow("input image", src);
		imshow("negative image", dst);
		waitKey();
	}
}

void testParcurgereSimplaDiblookStyle()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat dst = src.clone();

		double t = (double)getTickCount(); // Get the current time [s]

		// the fastest approach using the �diblook style�
		uchar *lpSrc = src.data;
		uchar *lpDst = dst.data;
		int w = (int)src.step; // no dword alignment is done !!!
		for (int i = 0; i<height; i++)
			for (int j = 0; j < width; j++) {
				uchar val = lpSrc[i*w + j];
				lpDst[i*w + j] = 255 - val;
			}

		// Get the current time again and compute the time difference [s]
		t = ((double)getTickCount() - t) / getTickFrequency();
		// Print (in the console window) the processing time in [ms] 
		printf("Time = %.3f [ms]\n", t * 1000);

		imshow("input image", src);
		imshow("negative image", dst);
		waitKey();
	}
}

void testColor2Gray()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname);

		int height = src.rows;
		int width = src.cols;

		Mat dst = Mat(height, width, CV_8UC1);

		// Asa se acceseaaza pixelii individuali pt. o imagine RGB 24 biti/pixel
		// Varianta ineficienta (lenta)
		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				Vec3b v3 = src.at<Vec3b>(i, j);
				uchar b = v3[0];
				uchar g = v3[1];
				uchar r = v3[2];
				dst.at<uchar>(i, j) = (r + g + b) / 3;
			}
		}

		imshow("input image", src);
		imshow("gray image", dst);
		waitKey();
	}
}

void testBGR2HSV()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname);
		int height = src.rows;
		int width = src.cols;

		// Componentele d eculoare ale modelului HSV
		Mat H = Mat(height, width, CV_8UC1);
		Mat S = Mat(height, width, CV_8UC1);
		Mat V = Mat(height, width, CV_8UC1);

		// definire pointeri la matricele (8 biti/pixeli) folosite la afisarea componentelor individuale H,S,V
		uchar* lpH = H.data;
		uchar* lpS = S.data;
		uchar* lpV = V.data;

		Mat hsvImg;
		cvtColor(src, hsvImg, CV_BGR2HSV);

		// definire pointer la matricea (24 biti/pixeli) a imaginii HSV
		uchar* hsvDataPtr = hsvImg.data;

		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				int hi = i*width * 3 + j * 3;
				int gi = i*width + j;

				lpH[gi] = hsvDataPtr[hi] * 510 / 360;		// lpH = 0 .. 255
				lpS[gi] = hsvDataPtr[hi + 1];			// lpS = 0 .. 255
				lpV[gi] = hsvDataPtr[hi + 2];			// lpV = 0 .. 255
			}
		}

		imshow("input image", src);
		imshow("H", H);
		imshow("S", S);
		imshow("V", V);

		waitKey();
	}
}

void testResize()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src;
		src = imread(fname);
		Mat dst1, dst2;
		//without interpolation
		resizeImg(src, dst1, 320, false);
		//with interpolation
		resizeImg(src, dst2, 320, true);
		imshow("input image", src);
		imshow("resized image (without interpolation)", dst1);
		imshow("resized image (with interpolation)", dst2);
		waitKey();
	}
}

void testCanny()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src, dst, gauss;
		src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		double k = 0.4;
		int pH = 50;
		int pL = (int)k*pH;
		GaussianBlur(src, gauss, Size(5, 5), 0.8, 0.8);
		Canny(gauss, dst, pL, pH, 3);
		imshow("input image", src);
		imshow("canny", dst);
		waitKey();
	}
}

void testVideoSequence()
{
	VideoCapture cap("Videos/rubic.avi"); // off-line video from file
	//VideoCapture cap(0);	// live video from web cam
	if (!cap.isOpened()) {
		printf("Cannot open video capture device.\n");
		waitKey(0);
		return;
	}

	Mat edges;
	Mat frame;
	char c;

	while (cap.read(frame))
	{
		Mat grayFrame;
		cvtColor(frame, grayFrame, CV_BGR2GRAY);
		Canny(grayFrame, edges, 40, 100, 3);
		imshow("source", frame);
		imshow("gray", grayFrame);
		imshow("edges", edges);
		c = cvWaitKey(0);  // waits a key press to advance to the next frame
		if (c == 27) {
			// press ESC to exit
			printf("ESC pressed - capture finished\n");
			break;  //ESC pressed
		};
	}
}


void testSnap()
{
	VideoCapture cap(0); // open the deafult camera (i.e. the built in web cam)
	if (!cap.isOpened()) // openenig the video device failed
	{
		printf("Cannot open video capture device.\n");
		return;
	}

	Mat frame;
	char numberStr[256];
	char fileName[256];

	// video resolution
	Size capS = Size((int)cap.get(CV_CAP_PROP_FRAME_WIDTH),
		(int)cap.get(CV_CAP_PROP_FRAME_HEIGHT));

	// Display window
	const char* WIN_SRC = "Src"; //window for the source frame
	namedWindow(WIN_SRC, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_SRC, 0, 0);

	const char* WIN_DST = "Snapped"; //window for showing the snapped frame
	namedWindow(WIN_DST, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_DST, capS.width + 10, 0);

	char c;
	int frameNum = -1;
	int frameCount = 0;

	for (;;)
	{
		cap >> frame; // get a new frame from camera
		if (frame.empty())
		{
			printf("End of the video file\n");
			break;
		}

		++frameNum;

		imshow(WIN_SRC, frame);

		c = cvWaitKey(10);  // waits a key press to advance to the next frame
		if (c == 27) {
			// press ESC to exit
			printf("ESC pressed - capture finished");
			break;  //ESC pressed
		}
		if (c == 115){ //'s' pressed - snapp the image to a file
			frameCount++;
			fileName[0] = NULL;
			sprintf(numberStr, "%d", frameCount);
			strcat(fileName, "Images/A");
			strcat(fileName, numberStr);
			strcat(fileName, ".bmp");
			bool bSuccess = imwrite(fileName, frame);
			if (!bSuccess)
			{
				printf("Error writing the snapped image\n");
			}
			else
				imshow(WIN_DST, frame);
		}
	}

}

void MyCallBackFunc(int event, int x, int y, int flags, void* param)
{
	//More examples: http://opencvexamples.blogspot.com/2014/01/detect-mouse-clicks-and-moves-on-image.html
	Mat* src = (Mat*)param;
	if (event == CV_EVENT_LBUTTONDOWN)
	{
		printf("Pos(x,y): %d,%d  Color(RGB): %d,%d,%d\n",
			x, y,
			(int)(*src).at<Vec3b>(y, x)[2],
			(int)(*src).at<Vec3b>(y, x)[1],
			(int)(*src).at<Vec3b>(y, x)[0]);
	}
}

void testMouseClick()
{
	Mat src;
	// Read image from file 
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		//Create a window
		namedWindow("My Window", 1);

		//set the callback function for any mouse event
		setMouseCallback("My Window", MyCallBackFunc, &src);

		//show the image
		imshow("My Window", src);

		// Wait until user press some key
		waitKey(0);
	}
}

/* Histogram display function - display a histogram using bars (simlilar to L3 / PI)
Input:
name - destination (output) window name
hist - pointer to the vector containing the histogram values
hist_cols - no. of bins (elements) in the histogram = histogram image width
hist_height - height of the histogram image
Call example:
showHistogram ("MyHist", hist_dir, 255, 200);
*/
void showHistogram(const std::string& name, int* hist, const int  hist_cols, const int hist_height)
{
	Mat imgHist(hist_height, hist_cols, CV_8UC3, CV_RGB(255, 255, 255)); // constructs a white image

	//computes histogram maximum
	int max_hist = 0;
	for (int i = 0; i<hist_cols; i++)
		if (hist[i] > max_hist)
			max_hist = hist[i];
	double scale = 1.0;
	scale = (double)hist_height / max_hist;
	int baseline = hist_height - 1;

	for (int x = 0; x < hist_cols; x++) {
		Point p1 = Point(x, baseline);
		Point p2 = Point(x, baseline - cvRound(hist[x] * scale));
		line(imgHist, p1, p2, CV_RGB(255, 0, 255)); // histogram bins colored in magenta
	}

	imshow(name, imgHist);
}
float **points;
int N;

void readPoints(char *name)
{
	FILE* f = fopen(name, "r");
	int nr;
	fscanf(f, "%d \n", &nr);
	N = nr;
	points = (float**)malloc(nr * sizeof(float*));
	printf("Read %d", nr);
	for (int i = 0; i < nr; i++)
	{
		points[i] = (float*)malloc(2 * sizeof(float));
		fscanf(f, "%f%f", &points[i][0], &points[i][1]);
		printf("%f %f \n", points[i][0], points[i][1]);
	}


}
int width = 500;
int height = 500;

void model1(Mat input)
{
	float sumaX = 0, sumaY = 0, produs = 0, sumaPatrateX = 0;
	for (int i = 0; i < N; i++)
	{
		sumaX += points[i][0];
		sumaY += points[i][1];
		sumaPatrateX += points[i][0] * points[i][0];
		produs += points[i][0] * points[i][1];
	}
	float o1 = (N*produs - sumaX*sumaY) / (N*sumaPatrateX - sumaX*sumaX);
	float o0 = (sumaY - o1*sumaX) / ((float)N);
	float angle = atan(o1);
	printf("\n Angle %f", angle);

	angle = abs(angle);
	if (angle < PI / 4)
	{

		int y1 = o0;
		int y2 = o0 + o1 * width;
		line(input, Point(0, y1), Point(width, y2), (0, 0, 255), 3);
	}
	else
	{
		int x1 = -o0 / 01;
		int x2 = (height - o0) / o1;
		line(input, Point(x1, 0), Point(x2, height), 3);

	}
	printf("\n %f %f", o0, o1);


}
void model2(Mat input)
{
	float diferentaPatrate = 0, sumaX = 0, sumaY = 0, produs = 0, sumaPatrateX = 0;
	for (int i = 0; i < N; i++)
	{
		sumaX += points[i][0];
		sumaY += points[i][1];
		sumaPatrateX += points[i][0] * points[i][0];
		produs += points[i][0] * points[i][1];
		diferentaPatrate += points[i][1] * points[i][1] -
			points[i][0] * points[i][0];
	}
	float beta = ((float)-1 / 2)*atan2(2 * produs - 2 * sumaX*sumaY / ((float)N), diferentaPatrate + sumaX*sumaX / ((float)N) - sumaY*sumaY / ((float)N));
	float ro = (float)(cos(beta)*sumaX + sin(beta)*sumaY) / (N);

	int y1 = ro / sin(beta);
	int y2 = (ro - cos(beta)*width) / sin(beta);
	line(input, Point(0, y1), Point(width, y2), (0, 255, 0), 1);



}
void doMean()
{
	Mat img(width, height, CV_8UC3);
	img.setTo(255);
	readPoints("points0.txt");
	for (int i = 0; i < N; i++)
	{
		img.at<Vec3b>((int)points[i][1], (int)points[i][0]) = Vec3b(0, 0, 0);

	}
	imshow("peter", img);
	waitKey();
	model1(img);
	imshow("peter", img);
	waitKey();
	model2(img);
	imshow("peter", img);
	waitKey();



}


void lab2()
{
	int s = 2;
	float p = 0.99;
	float w = 0.4;
	float t = 10;
	float q = 0.8;
	Mat img = imread("lab2/points3.bmp", CV_LOAD_IMAGE_GRAYSCALE);
	Mat dst(img.rows, img.cols, CV_8UC1); //8bit unsigned 1 channel

	vector<Point> points;
	for (int i = 0; i < img.rows; i++)
	{
		for (int j = 0; j < img.cols; j++)
		{
			if (img.at<uchar>(i, j) == 0)
			{
				points.push_back(Point(j, i));
			}
		}
	}
	float N = log(1 - p) / log(1 - pow(q, s));
	float T = points.size()*q;
	float bestscore = 0;
	float bestA = 0, bestB = 0, bestC = 0;
	Point startP, stopP;
	for (int i = 0; i < ceil(N); i++)
	{
		int first = rand() % points.size();

		int last = rand() % points.size();
		while (first == last)
		{
			last = rand() % points.size();
		}
		float al = 0, bl = 0, cl = 0;
		Point one = points.at(first);
		Point two = points.at(last);
		al = one.y - two.y;
		bl = two.x - one.x;
		cl = one.x*two.y - two.x*one.y;
		float currentScore = 0;
		for (int j = 0; j < points.size(); j++)
		{
			Point currentP = points.at(j);
			float distance = abs(al*currentP.x + bl*currentP.y + cl) / (sqrt(pow(al, 2) + pow(bl, 2)));
			if (distance < t)
			{
				currentScore++;

			}
		}
		if (currentScore > bestscore)
		{
			bestA = al;
			bestB = bl;
			bestC = cl;
			bestscore = currentScore;


		}
		if (currentScore > T)
		{
			break;
		}

	}
	Point start(0, -bestC / bestB);
	Point stop(img.rows - 1, (-bestC - (img.rows - 1)*bestA) / bestB);
	line(img, start, stop, 0, 1);
	imshow("bara", img);
	waitKey();

}
void lab3()
{
	char fname[MAX_PATH];
	Mat src;
	while (openFileDlg(fname) == 0)
	{

	}




	src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
	imshow("image", src);
	waitKey();


	int diagonal = sqrt(pow(src.rows, 2) + pow(src.cols, 2));
	vector<Point> points;
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			if (src.at<uchar>(i, j) == 255)
			{
				points.push_back(Point(j, i));
			}
		}
	}
	Mat Hough(diagonal + 1, 360, CV_32SC1);
	Hough.setTo(0);
	for (int i = 0; i < points.size(); i++)
	{
		Point current = points.at(i);
		for (int teta = 0; teta < 360; teta++)
		{

			float ro = current.x*cos(teta*PI / 180) + current.y*sin(teta*PI / 180);
			if (ro >= 0 && ro <= diagonal)
			{
				int roInt = ro;
				Hough.at<int>(roInt, teta)++;
			}
		}
	}
	int maxi = 0;
	for (int i = 0; i <diagonal + 1; i++)
	{
		for (int j = 0; j < 360; j++)
		{
			if (Hough.at<int>(i, j) > maxi)
			{
				maxi = Hough.at<int>(i, j);

			}
		}
	}
	Hough.convertTo(Hough, CV_8UC1, 255.f / maxi);
	imshow("da", Hough);
	waitKey(0);
	int kernelSize = 7;
	vector<peak> peaks;
	for (int i = 0; i < diagonal + 1; i++)
	{
		for (int j = 0; j < 360; j++)
		{

			bool isMaxima = true;
			int maxValue = Hough.at<uchar>(i, j);
			if (maxValue > 0){
				for (int m = -kernelSize / 2; m < kernelSize / 2; m++)
				{
					for (int n = -kernelSize / 2; n < kernelSize / 2; n++)
					{
						int newI = i + m;
						int newJ = j + n;
						if (newI >= 0 && Hough.rows>newI && newJ >= 0 && newJ < Hough.cols && (m != 0 || n != 0))
						{

							if (Hough.at<uchar>(newI, newJ) > maxValue)
							{
								isMaxima = false;

							}
						}

					}
				}
				if (isMaxima &&maxValue>15){
					peak m;
					m.hval = maxValue;
					m.ro = i;
					m.theta = j;
					peaks.push_back(m);
				}
			}
		}
	}
	sort(peaks.begin(), peaks.end(), compareByLength);
	Mat finalu(src.rows, src.cols, CV_8UC3);
	//src.convertTo(Hough, CV_8UC3, 1);

	src.convertTo(src, CV_8UC3);
	width = finalu.cols;
	height = finalu.rows;
	for (int i = 0; i < 15; i++)
	{
		int ro = peaks.at(i).ro;
		int theta = peaks.at(i).theta;
		int y1 = ro / sin(theta);
		int y2 = (ro - cos(theta)*width) / sin(theta);
		line(src, Point(0, y1), Point(width, y2), (0, 0, 255), 1);
	}

	imshow("da", src);
	waitKey(0);

}
Mat lab5()
{
	char folder[256] = "faces";
	char fname[256];
	Mat all(400, 19 * 19, CV_8UC1);
	for (int k = 1; k <= 400; k++)
	{
		sprintf(fname, "%s/face%05d.bmp", folder, k);
		Mat img = imread(fname, 0);
		for (int i = 0; i < img.rows; i++)
		{
			for (int j = 0; j < img.cols; j++)
			{
				all.at<uchar>(k - 1, i * 19 + j) = img.at<uchar>(i, j);
			}
		}
	}
	return all;
}
Mat computeMean(Mat all)
{
	Mat averages(19 * 19, 1, CV_32F);


	for (int i = 0; i < 19; i++)
	{
		for (int j = 0; j < 19; j++)
		{
			float mean = 0;
			for (int k = 0; k < 400; k++)
			{
				mean += all.at<uchar>(k, i * 19 + j);

			}
			averages.at<float>(i * 19 + j, 0) = mean / 400;
		}
	}
	return averages;
}
void writeToCsv(char* fname, Mat src)
{
	ofstream out(fname);
	for (int i = 0; i < src.cols; i++)
	{
		for (int j = 0; j < src.rows; j++)
		{
			out << src.at<float>(j, i) << ",";

		}
		out << "\n";

	}
	out.close();

}
Mat covarianceMat(Mat all)
{
	Mat averages = computeMean(all);
	Mat covar(19 * 19, 19 * 19, CV_32F);
	for (int i = 0; i < 19; i++)
	{
		for (int j = 0; j < 19; j++)
		{
			for (int m = 0; m < 19; m++)
			{
				for (int n = 0; n< 19; n++)
				{
					float covariance = 0;;
					for (int k = 0; k < 400; k++)
					{
						covariance += (all.at<uchar>(k, i * 19 + j) - averages.at<float>(i * 19 + j, 0)) *(all.at<uchar>(k, m * 19 + n) - averages.at<float>(m * 19 + n, 0));

					}
					covar.at<float>(i * 19 + j, m * 19 + n) = covariance / 400;
				}
			}
		}
	}
	return covar;

}
Mat standard(Mat all)
{
	Mat averages = computeMean(all);
	Mat dev(19 * 19, 1, CV_32F);
	for (int i = 0; i < 19; i++)
	{
		for (int j = 0; j < 19; j++)
		{
			float deviation = 0;
			for (int k = 0; k < 400; k++)
			{
				deviation += pow(all.at<uchar>(k, i * 19 + j) - averages.at<float>(i * 19 + j, 0), 2);
			}
			dev.at<float>(i * 19 + j, 0) = sqrt(deviation) / 400;

		}
	}
	return dev;
}
Mat correlation(Mat all)
{
	Mat averages = computeMean(all);
	Mat dev = standard(all);
	Mat covariance = covarianceMat(all);
	Mat correlation(19 * 19, 19 * 19, CV_32F);
	for (int i = 0; i < 19; i++)
	{
		for (int j = 0; j < 19; j++)
		{
			for (int m = 0; m < 19; m++)
			{
				for (int n = 0; n < 19; n++)
				{
					double corr = covariance.at<float>(i * 19 + j, m * 19 + n) / (dev.at<float>(i * 19 + j, 0)*dev.at<float>(m * 19 + n, 0));
					correlation.at<float>(i * 19 + j, m * 19 + n) = corr / 400;
				}
			}
		}
	}
	return correlation;

}
void corrCoeff(Mat all)
{
	Mat coef(256, 256, CV_8UC1);
	coef.setTo(255);
	Point a(3, 10);
	Point b(15, 9);

	for (int k = 0; k < 400; k++)
	{
		coef.at<uchar>(all.at<uchar>(k, a.y * 19 + a.x),
			all.at<uchar>(k, b.y * 19 + b.x)) = 0;
	}



	imshow("Coef", coef);
	waitKey(0);
}
void gauss(Mat all)
{

	Mat averages = computeMean(all);
	Mat dev = standard(all);
	Mat Gauss(256, 256, CV_32F);
	Mat normal(256, 256, CV_8UC1);
	Point feat(5, 4);
	for (int i = 0; i < 256; i++)
	{
		Gauss.at<float>(i, 0) = exp(-pow(i - averages.at<float>(feat.y * 19 + feat.x, 0), 2) / (2 * dev.at<float>(feat.y * 19 + feat.x, 0)*dev.at<float>(feat.y * 19 + feat.x, 0))) /
			(sqrt(2 * PI)*dev.at<float>(feat.y * 19 + feat.x, 0));

	}
	float max = 0;
	for (int i = 0; i < 256; i++)
	{
		for (int j = 0; j < 256; j++)
		{
			if (Gauss.at<float>(i, j) > max)
			{

			}
		}
	}

	imshow("Gaus", Gauss);
	waitKey(0);


}
float distance(Mat feats, Mat centers, int D, int point, int center)
{
	float dist = 0;
	for (int i = 0; i < D; i++)
	{
		dist += pow((centers.at<float>(center, i) - feats.at<float>(point, i)), 2);
	}
	return sqrt(dist);

}void printFeaturesGray(Mat src, int k, vector<int> assigned)
{


	Mat nou(src.rows, src.cols, CV_8UC3);
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j< src.cols; j++)
		{
			int position = i * src.cols + j;
			int color = assigned.at(position) * 255 / k;
			nou.at<Vec3b>(i, j) = Vec3b(color, color, color);
		}
	}
	imshow("Result", nou);
	waitKey(0);


}
vector<int> lab6(Mat src, Mat features, int D, int k)
{
	//srand(time(NULL));


	int nrPoints = features.rows;
	//Mat features(nrPoints, D, CV_32F);
	vector<int> centers;
	Mat centerFeatures(k, D, CV_32F);
	vector<int> assigned(nrPoints);

	while (centers.size() < k)
	{
		int i = rand() % nrPoints;
		bool ok = true;
		for (int j = 0; j < centers.size(); j++)
		{
			if (centers.at(j) == i)
			{
				ok = false;
			}
		}
		if (ok)
		{
			centers.push_back(i);
			features.row(i).copyTo(centerFeatures.row(centers.size() - 1));
		}
	}
	bool fixed = true;
	while (fixed)
	{
		fixed = false;
		for (int i = 0; i < nrPoints; i++)
		{

			float minDist = distance(features, centerFeatures, D, i, assigned.at(i));
			for (int j = 0; j < k; j++)
			{
				float currentDistance = distance(features, centerFeatures, D, i, j);
				if (currentDistance < minDist)
				{
					assigned.at(i) = j;
					minDist = currentDistance;
				}
			}
		}

		int p;
		for (int i = 0; i < k; i++)
		{
			Mat current(1, D, CV_32F);
			current.setTo(0);
			int points = 0;
			for (int m = 0; m < features.rows; m++)
			{
				if (assigned.at(m) == i)
				{
					points++;
					for (int j = 0; j < D; j++)
					{
						current.at<float>(0, j) += features.at<float>(m, j);

					}
				}
			}
			//float distance(Mat feats, Mat centers, int D, int point,int center)

			current /= points;
			if (distance(current, centerFeatures.row(i), D, 0, 0) > 7)
			{
				fixed = true;
			}
			current.row(0).copyTo(centerFeatures.row(i));
		}

	}
	//printFeaturesGray(src, k, assigned);
	return assigned;
}
void printFeaturesColor(Mat src, int k, vector<int> assigned)
{
	//srand(time(NULL));
	vector<Vec3b> colors(k);
	for (int i = 0; i < k; i++)
	{
		float b = 0, g = 0, r = 0;
		int nr = 0;
		for (int n = 0; n < src.rows; n++)
		{
			for (int m = 0; m < src.cols; m++)
			{
				if (assigned.at(n*src.cols + m) == i)
				{
					b += src.at<Vec3b>(n, m)[0];
					g += src.at<Vec3b>(n, m)[1];
					r += src.at<Vec3b>(n, m)[2];
					nr++;
				}
			}
		}
		colors.at(i) = Vec3b(b / nr, g / nr, r / nr);

	}
	Mat nou(src.rows, src.cols, CV_8UC3);
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j< src.cols; j++)
		{
			int position = i * src.cols + j;

			nou.at<Vec3b>(i, j) = colors.at(assigned.at(position));
		}
	}
	imshow("Result", nou);
	waitKey(0);
}

void printFeaturesGrayscale(Mat src, int k, vector<int> assigned)
{
	//srand(time(NULL));
	vector<uchar> colors(k);
	for (int i = 0; i < k; i++)
	{
		int c = 0;
		int nr = 0;
		for (int n = 0; n < src.rows; n++)
		{
			for (int m = 0; m < src.cols; m++)
			{
				if (assigned.at(n*src.cols + m) == i)
				{
					c += src.at<uchar>(n, m);

					nr++;
				}
			}
		}
		colors.at(i) = c / nr;

	}
	Mat nou(src.rows, src.cols, CV_8UC1);
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j< src.cols; j++)
		{
			int position = i * src.cols + j;

			nou.at<uchar>(i, j) = colors.at(assigned.at(position));
		}
	}
	imshow("Result", nou);
	waitKey(0);
}

Mat readImage()
{

	char fname[MAX_PATH];
	Mat src;
	while (openFileDlg(fname) == 0)
	{

	}
	src = imread(fname, CV_LOAD_IMAGE_COLOR);
	imshow("image", src);
	waitKey();
	return src;
}
Mat features(Mat src)
{
	int nrPoints = src.rows;
	Mat nouveau(src.rows*src.cols, 3, CV_32F);

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			nouveau.at<float>(i*src.cols + j, 0) = src.at<Vec3b>(i, j)[0];
			nouveau.at<float>(i*src.cols + j, 1) = src.at<Vec3b>(i, j)[1];
			nouveau.at<float>(i*src.cols + j, 2) = src.at<Vec3b>(i, j)[2];

		}
	}
	return nouveau;
}

Mat getIntensity(Mat src)
{
	int nrPoints = src.rows;
	Mat nouveau(src.rows* src.cols, 1, CV_32F);

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			nouveau.at<float>(i*src.cols + j, 0) = src.at<uchar>(i, j);



		}
	}
	return nouveau;
}
Mat getFeatures(Mat src)
{
	int nrPoints = src.rows;
	Mat nouveau(src.rows* src.cols, 2, CV_32F);

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			nouveau.at<float>(i*src.cols + j, 0) = i;
			nouveau.at<float>(i*src.cols + j, 1) = j;


		}
	}
	return nouveau;
}
Mat readPoints()
{
	char fname[MAX_PATH];
	Mat src;
	while (openFileDlg(fname) == 0)
	{

	}
	ifstream fin(fname);
	int nr;
	int D;
	fin >> nr;
	fin >> D;
	Mat features(nr, D, CV_64FC1);
	features.setTo(0);
	for (int i = 0; i < nr; i++)
	{
		for (int j = 0; j < D; j++)
		{
			fin >> features.at<double>(i, j);
		}
	}
	return features;

}
Mat means(Mat src)
{
	Mat mn(src.cols, 1, CV_64FC1);
	mn.setTo(0);
	for (int i = 0; i < src.cols; i++)
	{
		double mean = 0;
		for (int j = 0; j < src.rows; j++)
		{
			mean += src.at<double>(j, i);
		}
		mn.at<double>(i, 0) = mean / (double)src.rows;
	}
	return mn;

}
Mat subtractMean(Mat src, Mat mean)
{
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			src.at<double>(i, j) -= mean.at<double>(j, 0);
		}
	}
	return src;
}
double pointDiff(Mat a, Mat b)
{
	float diff = 0;
	for (int  i = 0; i < a.cols; i++)
	{
		diff += pow(a.at<double>(i)-b.at<double>(i), 2);
	}
	return sqrt(diff);
}
double meanDifference(Mat src, Mat transformed)
{
	double averageMean = 0;
	for (int  i = 0; i < src.rows; i++)
	{
		
		averageMean+=pointDiff(src.row(i), transformed.row(i));

		
	}
	return averageMean / src.rows;


}
Mat extremes(Mat src)
{
	Mat tmp(2, src.cols, CV_64FC1);
	
	
	for (int j = 0; j < 2; j++)
	{
		src.row(0).copyTo(tmp.row(j));
	}
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			if (tmp.at<double>(0, j) > src.at < double>(i, j))
			{
				tmp.at<double>(0, j) = src.at < double>(i, j);
			}
			if (tmp.at<double>(1, j) < src.at < double>(i, j))
			{
				tmp.at<double>(1, j) = src.at < double>(i, j);
			}
			
		}
	}
	return tmp;
}
void lab7()
{
	Mat points = readPoints();
	Mat mean = means(points);
	Mat XI = subtractMean(points, mean);
	Mat C = XI.t()*XI / (points.rows);
	Mat Lambda, Q;
	eigen(C, Lambda, Q);
	Q = Q.t();
	int k = 3;
	double eigenSum = 0;

	
	for (int i = 0; i < Lambda.rows; i++)
	{
		cout << Lambda.at<double>(i)<<" ";
	}
	Rect roi(0, 0, k, Q.rows);
	Mat newQ(Q, roi);
	Mat xFeats = points*newQ;
	Mat xAprox = xFeats*newQ.t();
	double meanu = meanDifference(points, xAprox);
	Mat extrem = extremes(xFeats);
	
	Mat newPoints = subtractMean(xFeats, extrem.row(0).t());
	 extrem = extremes(xFeats);
	Mat disp(extrem.at<double>(1, 0) + 1, extrem.at<double>(1, 1)+1, CV_8UC1);
	disp.setTo(255);
	for (int i = 0; i < newPoints.rows; i++)
	{
		disp.at<uchar>((int)newPoints.at<double>(i, 1), (int)newPoints.at<double>(i, 0)) = (int) newPoints.at<double>(i, 2) * 255/(extrem.at<double>(1, 2) - extrem.at<double>(0, 2));
	}
	imshow("Test",disp);
	waitKey(0);


}
Mat colorHist(Mat src, int m )
{
	
	
	Mat histogramn( 3, m,CV_32F);
	histogramn.setTo(0);
	int bin_size = 256 / m;
	for (int  i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			Vec3b aux = src.at<Vec3b>(i, j);
			histogramn.at<float>(0,min(m-1,(int) aux[0] / bin_size))++;
			histogramn.at<float>(1, min(m-1,(int)aux[1] / bin_size))++;
			histogramn.at<float>(2,min(m-1, (int) aux[2] / bin_size))++;

		}

	}
	Mat hist(1, m * 3, CV_32F);
	for (int i = 0; i < histogramn.rows; i++)
	{
		for (int j = 0; j < histogramn.cols; j++)
		{
			hist.at<float>(i*histogramn.cols + j) = histogramn.at<float>(i, j);
		}
	}
	return hist;
	
}
Mat readImages()
{
	const int nrclasses = 6;
	char classes[nrclasses][10] =
	{ "beach", "city", "desert", "forest", "landscape", "snow" };
	int nrinst = 676;
	int feature_dim = 3;
	Mat X(nrinst, feature_dim, CV_32FC1);
	Mat y(nrinst, 1, CV_8UC1);
	char fname[MAX_PATH];
	int c = 0, fileNr = 0, rowX = 0;
	int nrBeans = 8;
	while (1) {
		sprintf(fname, "train/%s/%06d.jpeg", classes[c], fileNr++);
		Mat img = imread(fname);
		if (img.cols == 0) break;
		//calculate the histogram in hist
		Mat histogram = colorHist(img, nrBeans);
		Mat hist( nrBeans*3,1,CV_32F);
		//imshow("test", img);
		//waitKey(0);
		for (int i = 0; i < img.rows; i++)
		{
			for (int j = 0; j < img.cols; j++)
			{
				hist.at<float>(i*img.cols + j) = hist.at<float>(i, j);
			}
		}
		
		
		for (int d = 0; d < hist.cols; d++)
			X.at<float>(rowX, d) = hist.at<float>(d);

		y.at<uchar>(rowX) = c;
		rowX++;
	}
	return X;
}
bool mySort(Neighbor i, Neighbor j) {

	return i.distance >j.distance;
}
double distanceBetween(Mat a, Mat b)
{
	double distance = 0;
	for (int i = 0; i < a.cols; i++)
	{
		distance += pow(a.at<float>(i) - b.at<float>(i), 2);

	}
	return sqrt(distance);
}

void classify()
{
	const int nrclasses = 6;
	int imgFeatures = 3;
	char classes[nrclasses][10] =
	{ "beach", "city", "desert", "forest", "landscape", "snow" };
	int nrinst = 672;
	int nrBeans = 16;

	int feature_dim = imgFeatures * nrBeans;

	Mat X(nrinst, feature_dim, CV_32FC1);
	Mat y(nrinst, 1, CV_8UC1);
	char fname[MAX_PATH];
	int rowX = 0;
	for (int c = 0; c < nrclasses; c++)
	{


		int fileNr = 0;
		while (1) {
			sprintf(fname, "train/%s/%06d.jpeg", classes[c], fileNr++);
			Mat img1 = imread(fname);
			Mat img;

			if (img1.cols == 0) break;
			cvtColor(img1, img, CV_BGR2Lab);

			//calculate the histogram in hist
			//imshow("test", img);
			//waitKey(0);


			Mat hist = colorHist(img, nrBeans);
			for (int d = 0; d < hist.cols; d++)
				X.at<float>(rowX, d) = hist.at<float>(d);

			y.at<uchar>(rowX) = c;
			rowX++;
		}
	}
	int k = 15;
	int nrCases = 0;
	int goodCases = 0;
	Mat confussion(nrclasses, nrclasses, CV_8UC1);
	confussion.setTo(0);
	for (int c = 0; c < nrclasses; c++)
	{


		int fileNr = 0;
		while (1)
		{
			std::priority_queue<Neighbor> neighbors;
			char fileName[MAX_PATH] ;
			sprintf(fileName, "test/%s/%06d.jpeg", classes[c], fileNr++);

			Mat img1 = imread(fileName);
			Mat img;

			if (img1.cols == 0) break;
			cvtColor(img1, img, CV_BGR2Lab);

			nrCases++;
			Mat histo = colorHist(img, nrBeans);

			for (int i = 0; i < X.rows; i++)
			{

				if (img.cols == 0) break;
				int c = y.at<uchar>(i);
				double distance = distanceBetween(X.row(i), histo);
				neighbors.push(Neighbor(y.at<uchar>(i), distance));
			}
			vector<int> matches(nrclasses);
			fill(matches.begin(), matches.end(), 0);

			for (int i = 0; i < k; i++)
			{
				Neighbor aux = neighbors.top();
				neighbors.pop();
				matches.at(aux.label)++;
			}
			int pos = -1, max = -1;

			for (int i = 0; i < matches.size(); i++)
			{
				if (matches.at(i) > max)
				{
					max = matches.at(i);
					pos = i;
				}

			}
			//printf("The elements is %s", classes[pos]);
			if (pos == c)
			{
				goodCases++;
				
			}
			confussion.at<uchar>(pos, c)++;



		}
	}
	printf("\n\n");
	for (int  i = 0; i < confussion.rows; i++)
	{
		for (int j = 0; j < confussion.cols; j++)
		{
			printf("%d ", confussion.at<uchar>(i, j));
		}
		printf("\n");
	}
	
	printf("\nThe total accuracy is %f", (float)goodCases / nrCases);

	
}
Mat bayesImages()
{
	int nrImages = 60000;
	int nrFeatures = 28 * 28;
	int nrclasses = 10;
	Mat features(nrImages, nrFeatures+1, CV_8UC1);
	features.setTo(0);
	int current = 0;
	char fname[MAX_PATH];
	for (int c = 0; c < nrclasses; c++)
	{
		int fileNr = 0;
		while (1) {
			sprintf(fname, "bayes/train/%d/%06d.png", c, fileNr++);
			Mat img = imread(fname);
		
			printf("Sunt la %d \n", current);
			if (img.cols == 0) break;
			for (int i = 0; i < img.rows; i++)
			{
				for (int j = 0; j < img.cols; j++)
				{

					features.at<uchar>(current, i*img.cols+j) = (img.at<uchar>(i, j) > 128) ? 255 : 0;
				}
			}

			features.at<uchar>(current, nrFeatures) = c;
			int m = features.at<uchar>(current, nrFeatures);
			current++;
		}
	}
	imwrite("features.png", features);
	return features;
	
}
Mat bayesMat()
{
	
	Mat img = imread("features.png",CV_8UC1);
	int nrFeatures = 28 * 28;
	int nrclasses = 10;
	int nrImages = 60000;

	Mat features(nrFeatures, nrclasses, CV_32F);
	features.setTo(0);
	for (int c = 0; c < nrclasses; c++)
	{
		printf("Processing class %d", c);
		int count = 1;
		int nrInstances = 10;
		for (int i = 0; i < img.rows; i++)
		{
			int r = img.at<uchar>(i, nrFeatures);
			if (img.at<uchar>(i, img.cols - 1) == c)
			{
				nrInstances++;
			}
		}
		for (int j = 0; j < nrFeatures; j++)
		{
			for (int i = 0; i < nrImages; i++)
			{
				if (img.at<uchar>(i, j) == 255 && img.at<uchar>(i, nrFeatures) == c)
				{
					count++;
				}
			}
			features.at<float>(j,c)=count / (double)nrInstances;
		}
		

		
	}
	
	imwrite("bayesian.jpg", features);
	return features;
}
char bayesClassify(Mat imagePixels,Mat features, Mat src)
{
	 features = imread("bayesian.jpg", CV_32F);

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			src.at<uchar>(i, j) = (src.at<uchar>(i, j) > 128) ? 255 : 0;
		}
	}
	int nrclasses = 10;
	Mat scores(1, nrclasses, CV_32F);
	int n = imagePixels.depth();
	imagePixels.at<uchar>(1, 1);
	for (int c = 0; c < nrclasses; c++)
	{
		int count = 1;
		int total = nrclasses;
		for (int j = 0; j < features.cols; j++)
		{
			for (int k = 0; k < features.rows; k++)
			{
				total++;
				if (imagePixels.at<uchar>(k, 28 * 28) == c)
				{
					count++;
				}

			}
		}
		scores.at<float>(0,c) = log(count / (double)total);
	}
	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			for (int c = 0; c < nrclasses	; c++)
			{
				float test = features.at<float>(i*src.cols + j, c);
				scores.at<float>(0, c) += log((src.at<uchar>(i, j) == 255) ? features.at<float>(i*src.cols + j, c) : 1 - features.at<float>(i*src.cols + j, c));;
			}

		}

	}
	float  min= -FLT_MAX;
	int clas = -1;
	for (int c = 0; c < nrclasses; c++)
	{
		int r = scores.at<float>(0, c);
		if (scores.at<float>(0,c) > min)
		{
			min = scores.at<float>(0,c);
			clas = c;
		}
	}
	return clas;

	
	
}
void Bayes()
{
	Mat pictures = imread("features.png",CV_8UC1);

	
	

	char fname[MAX_PATH];
	int nrclasses = 10;
	for (int c = 0; c < nrclasses; c++)
	{
		int fileNr = 0;
		while (1) {
			sprintf(fname, "bayes/test/%d/%06d.png", c, fileNr++);
			Mat img = imread(fname);

			if (img.cols == 0) break;
			printf("The image is %d\n", bayesClassify(pictures,img, img));
			imshow("Test", img);

			waitKey(0);
			
		}
	}

}

vector<MyP> readDisparity(Mat in)
{
	vector<MyP> points;
	int B = 218;
	int F = 811;
	for (int i = 0; i < in.rows; i++)
	{
		for (int  j = 0; j < in.cols; j++)
		{
			if (in.at<uchar>(i, j) != 1)
			{
				float x = 0, y = 0, z = 0;
				float value = in.at<uchar>(i, j) / 4.0;

				z = B * F / value;
				x = (in.cols / 2 - j) *z / F;
				y = (in.rows / 2 - i)*z / F;
				MyP newPoint(x, y, z);
				points.push_back(newPoint);
			}

		}
	}
	return points;
}
void CCKNN()
{
	//srand(time(NULL));
	int width=100, int height = 100;
	Mat po(height, width, CV_32F);
	Mat in = imread("D000001.bmp");

	vector<MyP> points=readDisparity(in);
	
	vector<MyP> plan;
	int nrPoints=3;
	for (int i = 0; i < nrPoints; i++)
	{
		bool unique = false;
		int m;
		while (!unique)
		{
			unique = true;
			 m = rand() % points.size();
			for (int i = 0; i < plan.size(); i++)
			{
				if (plan.at(i) == points.at(m))
				{
					unique = false;
				}
			}
		}
		plan.push_back(points.at(m));

	}
	MyP AB = plan.at(0) - plan.at(1);
	MyP AC = plan.at(0) - plan.at(1);
	
	





	

}









int main()
{
	//srand(time(NULL));
	int op;
	CCKNN();
	
	return 0;

}